#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import folium
from geopy.geocoders import Nominatim
import pandas as pd
import folium.plugins
from folium.plugins import MarkerCluster

df1_Coord_brut = pd.read_csv('Scrap_Monster_Clean.csv')
colonnes_a_garder = ["mot_clef", "intitule_offre", "ville", "lien_offre", "longitude", "latitude"]
df1_Coord_propres = df1_Coord_brut[colonnes_a_garder]
donnees_groupees = df1_Coord_propres.groupby("mot_clef")
Mots_clefs = donnees_groupees.groups

def carto(df1_Coord_propres):  
    '''Crée une carte folium à partir du dataframe en affichant les liens d'offres et les types de metiers par ville'''
    geolocator = Nominatim(user_agent="cartographie-pav")
    centre1 = geolocator.geocode("Auvergne-Rhône-Alpes", addressdetails=True)
    centre1_coordonnees = (centre1.latitude, centre1.longitude)
    carte = folium.Map(location=centre1_coordonnees, zoom_start=12)
    
    LAT_GRE = 45.188529
    LONG_GRE = 5.724524
    folium.Circle(
    radius=15000,
    location= [LAT_GRE, LONG_GRE],
    color="crimson",
    fill=False,
    ).add_to(carte)

    couleurs_Types = {
        "'data-analyst'": "red",
        "'data-ingenieur'": "orange",
        "'data-scientist'": "green",
        "'developpeur-data'": "blue",
    
    }

    for mot_clef in donnees_groupees.groups: 
        groupe = donnees_groupees.get_group(mot_clef)
        couleur = couleurs_Types[mot_clef]
        emplacements = []
        descriptions = []
        icones = []
     
        for _, pav in groupe.iterrows():
            intitule_offre = pav["intitule_offre"]    
            longitude = pav["longitude"]
            latitude = pav["latitude"]

            emplacements.append([latitude, longitude])    
            descriptions.append(folium.Popup(intitule_offre))
            icones.append(folium.Icon(color=couleur))

        folium.plugins.MarkerCluster(
            name=mot_clef,
            locations=emplacements,
            popups=descriptions,
            icons=icones,
        ).add_to(carte)

    folium.LayerControl().add_to(carte)

    return carte

