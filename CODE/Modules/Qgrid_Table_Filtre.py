#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import qgrid

def qgrid_table():
    df=pd.read_csv('Scrap_Monster_Clean.csv')
    for row in df.iterrows():
        df['langage_prog']= df['langage_1'].map(str) +","+ df['langage_2'].map(str) +","+ df['langage_3'].map(str) +","+ df['langage_4'].map(str)
        df.to_csv('Monster.csv', index=False)
    df = df.drop(["langage_1", "langage_4", "langage_2", "langage_3", "description", "longitude", "latitude"], axis = 1)
    df2_qgrid=qgrid.show_grid(df, show_toolbar=True)
    return df2_qgrid   

