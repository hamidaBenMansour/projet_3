#!/usr/bin/env python
# coding: utf-8

# In[1]:


import folium
import folium.plugins
from geopy.geocoders import Nominatim
import pandas as pd
import os
from folium.plugins import MarkerCluster

LAT_GRE = 45.188529
LONG_GRE = 5.724524
maps = folium.Map(location=[LAT_GRE, LONG_GRE],zoom_start=12)
marker_cluster=MarkerCluster()
df_clean_visu = pd.read_csv('Scrap_Monster_Clean.csv')

def carto(df_clean_visu):
    for row in df_clean_visu.itertuples():  
        folium.Marker(location=[row.latitude, row.longitude], popup= f'<a href="{row.lien_offre}">lien</a>' +"\n \n" + row.intitule_offre + "\n \n" ).add_to(marker_cluster)
        marker_cluster.add_to(maps)
        
    folium.Circle(
        radius= 15000,
        location= [LAT_GRE, LONG_GRE],
        color= 'darkblue',
        fill=False,
    ).add_to(maps)

    return maps

