#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np

import matplotlib.pyplot as plt
import plotly.express as px
import pandas as pd
import psycopg2
from psycopg2.extensions import parse_dsn


# In[2]:


def radar_mots_clefs():
    
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    
    df= pd.read_csv("Scrap_Monster_Clean.csv")
    conn.commit()
    
    dict_df = dict(df['mot_clef'].value_counts())


    df_for_radar = pd.DataFrame(dict(
    r=dict_df.values(),
    theta=dict_df.keys()))
    fig = px.line_polar(df_for_radar, r='r', theta='theta', line_close=True)
    fig.show()

