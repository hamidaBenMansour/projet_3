#!/usr/bin/env python
# coding: utf-8

# In[2]:


import matplotlib
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

df_Clean = pd.read_csv('Scrap_Monster_Clean.csv')
def visiu1():

    DF1 = df_Clean[["mot_clef", "langage_1", "langage_2", "langage_3", "langage_4"]].groupby(["mot_clef"]).count()
    DF1.to_csv('langage.csv', index=None)
    df1 = pd.read_csv('langage.csv')
    Type_metier =["'data-analyst'", "'data-ingenieur'", "'data-scientist'", "'developpeur-data'"]
    df1['Metier'] = Type_metier
    df1= df1.rename(columns={'langage_1': 'SQL','langage_2': 'Python','langage_3': 'SCALA','langage_4': 'JAVA'})
   
    x = np.arange(len(df1['Metier']))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, df1['SQL'], width, label='sql')
    rects2 = ax.bar(x + width/2, df1['Python'], width, label='python')
    rects1 = ax.bar(x - width/2, df1['SCALA'], width, label='scala')
    rects2 = ax.bar(x + width/2, df1['JAVA'], width, label='java')

# Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Scores')
    ax.set_title('Scores par type de metiers et langages ')
    ax.set_xticks(x)
    ax.set_xticklabels(df1['Metier'])
    ax.legend()


    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')


    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()

    plt.show()


# In[3]:


visiu1()


# In[ ]:




