
#!/bin/bash

# Ce script automatise un pipeline de données.
#Utilisation:./pipeline.sh
sudo docker-compose down
sudo docker-compose up -d
sudo docker-compose ps
pipenv install
pipenv run python3 SCRAP_MONSTER.py
pipenv run python3 Creation_Database.py
pipenv run python3 Suppression_Doublons.py
pipenv run python3 Vérif_Qualité_données-Scrap.py
pipenv run python3 Coord_Monster.py
pipenv run python3 Carto_Monster.py
pipenv run python3 Jupyter_Final.py
sudo docker-compose down

