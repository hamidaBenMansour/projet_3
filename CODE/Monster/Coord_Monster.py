#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from geopy.exc import GeocoderTimedOut 
from geopy.geocoders import Nominatim 
import pandas as pd
import numpy as np

longitude = [] 
latitude = [] 
df1 = pd.read_csv('Scp_MONSTER.csv')
def findGeocode(lieu): 
    '''Cette fonction permet de recuperer les coordonées geographiques pour tous les villes dans la dataframe'''
    try: 
        geolocator = Nominatim(user_agent="cartographie-pav") 
        return geolocator.geocode(lieu) 
    except GeocoderTimedOut: 
        return findGeocode(lieu)

for i in (df1["lieu"]): 
    if findGeocode(i) != None: 
        lieu = findGeocode(i) 
        latitude.append(lieu.latitude) 
        longitude.append(lieu.longitude)
    else: 
        latitude.append(np.nan) 
        longitude.append(np.nan) 
        
for i in range (len(df1["longitude"])):
    if np.isnan(float(df1["longitude"][i])):   
        df1["longitude"][i]=longitude[i] 
        
for i in range (len(df1["latitude"])):
    if np.isnan(float(df1["latitude"][i])):
        df1["latitude"][i]=latitude[i] 
               
df1.to_csv('Coord.csv', index=None)      


# In[7]:


df1 


# In[9]:


df1.info()

