#!/usr/bin/env python
# coding: utf-8

# In[3]:


import re
import time
import requests as rq
from bs4 import BeautifulSoup as bs
import pandas as pd
import numpy as np

Scrap_Monster = {"mot_clef":[], "intitule_offre": [], "nom_entreprise": [], 
                 "lieu": [], "DATE": [], "lien": [], 
                 'latitude':[], 'longitude':[], 'description':[], 
                 'Type-contrat':[], 'langage1':[], 'langage2':[], 'langage3':[], 'langage4':[] }  
list_clefs = ["'data-analyst'", "'data-scientist'", "'developpeur-data'", "'data-ingenieur'"]
def Scrapping_Monster():
    '''permet de scrapper le site monster, recuperer les données, créer une dataframe et un fichier csv'''
    for mot_clef in list_clefs:
        url_sc_monster = f'https://www.monster.fr/emploi/recherche/?q={mot_clef}&where=Auvergne__2DRh__C3__B4ne__2DAlpes&cy=fr&stpage=1&page=9'   
        r = rq.get(url_sc_monster, auth=('user', 'pass'))
        r.status_code
        r.headers['content-type']
        soup = bs(r.text, 'html.parser')
        time.sleep(0.5)
        sc_intitule = soup.find_all("h2", class_ = "title")
        sc_nom_entreprise= soup.find_all("div", class_ = "company")
        sc_lien = soup.find_all("h2", class_ = "title")
        sc_lieu = soup.find_all("div", class_ = "location")
        sc_autre_lieu = soup.find_all("span", class_ = "mux-tooltip multi-loc-link")
        sc_coord =soup.find_all('a',{"data-bypass":"true"})
        for i in range(len(soup.find_all("div", {"class":"summary"}))-1):
            Scrap_Monster["mot_clef"].append(mot_clef)
            Scrap_Monster["intitule_offre"].append(sc_intitule[i].text.replace("\n", "").strip())        
            Scrap_Monster["lien"].append(sc_lien[i].find('a').get('href'))      
            Scrap_Monster["nom_entreprise"].append(sc_nom_entreprise[i].text.replace("\n", " ").strip())      
            Scrap_Monster["lieu"].append(sc_lieu[i+1].text.replace("\n", "").strip().replace(", Auvergne-Rhône-Alpes", " "))
            Scrap_Monster['latitude'].append(str(sc_coord[i].get('data-m_impr_j_lat')))
            Scrap_Monster['longitude'].append(str(sc_coord[i].get('data-m_impr_j_long')))
 
            regex = re.compile("\d{4}-\d{2}-\d{2}T\d{2}:\d{2}")
            date = regex.findall(str(sc_coord[i]))
            Scrap_Monster["DATE"].append(date[0] if len(date)>0 else np.nan) 
            url_offre = sc_lien[i].find('a').get('href')
            r = rq.get(url_offre)
            soupet = bs(r.text, 'html.parser')
            sc_description_raw = soupet.find("div", class_ = "job-description")
            if  sc_description_raw is None:
                Scrap_Monster["description"].append(None)
            else:
                sc_description = sc_description_raw.text
                Scrap_Monster["description"].append(sc_description)
            regex = re.compile("((CDI)|(CDD)|(stage))")
            type_contrat = re.findall(regex, str(Scrap_Monster["description"][i]))                                      
            if len(type_contrat)>0: 
                Scrap_Monster["Type-contrat"].append(list(type_contrat)[0][0])
            else:
                Scrap_Monster["Type-contrat"].append(np.nan)              
            pattern1 = re.compile(r'(?P<sql>[Ss][Qq][Ll])')
            pattern2 = re.compile(r'(?P<python>[Pp][Yy][Tt][Hh][Oo][Nn])')
            pattern3 = re.compile(r'(?P<scala>[Ss][Cc][Aa][Ll][Aa])')
            pattern4 = re.compile(r'(?P<java>[Jj][Aa][Vv][Aa])')
            match1 = re.search(pattern1,sc_description)
            match2 = re.search(pattern2,sc_description)
            match3 = re.search(pattern3,sc_description)
            match4 = re.search(pattern4,sc_description)
        
            if match1 is None:
                Scrap_Monster["langage1"].append(np.nan)    
            else:
                Scrap_Monster["langage1"].append(match1.group()) 
            if match2 is None:
                Scrap_Monster["langage2"].append(np.nan)
            else:   
                Scrap_Monster["langage2"].append(match2.group())    
            
            if match3 is None:
                Scrap_Monster["langage3"].append(np.nan)
            else:    
                Scrap_Monster["langage3"].append(match3.group())
            if match4 is None:
                Scrap_Monster["langage4"].append(np.nan)
            else:   
                Scrap_Monster["langage4"].append(match4.group())
        
    df = pd.DataFrame(Scrap_Monster)
    df1 = df.astype(object).replace('None', np.nan)
    df1.to_csv("Scp_MONSTER.csv", index =False)
    return (df1)


# In[4]:


Scrapping_Monster()


# In[ ]:




