#!/usr/bin/env python
# coding: utf-8

# In[282]:


import pandas as pd

df_Sans_doublons = pd.read_csv('Scrap_MONSTER_sans_doublon.csv')
df_Sans_doublons 


# ### Cette fonction nous renvoie les valeurs uniques présentées 

# In[283]:


df_Sans_doublons.lieu.unique()


# In[284]:


df_Sans_doublons.intitule_offre.unique()


# ### Cette fonction nous fournit pleins de données très utiles sur la répartition de nos données 

# In[285]:


df_Sans_doublons.describe(include="all")


# ### Cette fonction renvoie un DataFrame où toutes les  NaN  dans la colonne  DATE on été remplacés par 0.

# In[286]:


df_Sans_doublons.DATE.head(10)


# In[287]:


df_Sans_doublons.fillna(value={"DATE": 0}).DATE.head(10)


# In[288]:


df_Sans_doublons.fillna(method="pad").DATE.head(10)


# In[289]:


df_Sans_doublons.info()


# ### la répartition des valeurs manquantes dans le jeu de données à l'aide de la fonction heatmap() de seaborn et de la méthode de dataframe isnull()

# In[290]:


import seaborn as sns
sns.heatmap(df_Sans_doublons.isnull(), cbar=False)


# ###  Le nombre de valeurs manquantes par ligne

# In[291]:


df_Sans_doublons.isnull().sum(axis=1).value_counts()


# ### Afficher du nombre de valeurs par catégorie à l'aide de pandas

# In[293]:


cat_col = ["nom_entreprise", "lieu", "Type-contrat", "langage1", "langage2", "langage3", "langage4"]
for col in cat_col:
    print(df_Sans_doublons[col].value_counts())


# In[294]:


Modif_lang1=df_Sans_doublons["langage1"].str.upper()


# In[295]:


Modif_lang1


# In[296]:


Modif_lang1.value_counts()


# In[297]:


Modif_lang2=df_Sans_doublons["langage2"].str.upper()


# In[298]:


Modif_lang2


# In[299]:


Modif_lang2.value_counts()


# In[300]:


Modif_lang3=df_Sans_doublons["langage3"].str.upper()


# In[301]:


Modif_lang3.value_counts()


# In[302]:


Modif_lang4=df_Sans_doublons["langage4"].str.upper()


# In[303]:


Modif_lang4.value_counts()


# ### Regrouper les catégories clermont-ferrand et Clermont-Ferrand en une seule

# In[304]:


df_Sans_doublons.loc[df_Sans_doublons['lieu'] == 'clermont-ferrand'] = 'Clermont-Ferrand '
Modif_lieu=df_Sans_doublons['lieu']
Modif_lieu


# ### Créer 3 nouveaux colonnes: "langage_1", "langage_2" et "ville" dans la dataframe
# ### Cette fonction "drop()" permet de supprimer les colonnes où on a modifé  les données afin d'amelioer la qualité de donnée

# In[305]:


df_Sans_doublons['langage_1'] = Modif_lang1
df_Sans_doublons


# In[306]:


df_Sans_doublons.drop("langage1", axis=1, inplace=True)
df_Sans_doublons


# In[307]:


df_Sans_doublons['langage_2'] = Modif_lang2
df_Sans_doublons


# In[308]:


df_Sans_doublons.drop("langage2", axis=1, inplace=True)
df_Sans_doublons


# In[309]:


df_Sans_doublons['langage_3'] = Modif_lang3
df_Sans_doublons


# In[310]:


df_Sans_doublons.drop("langage3", axis=1, inplace=True)
df_Sans_doublons


# In[311]:


df_Sans_doublons['langage_4'] = Modif_lang4
df_Sans_doublons


# In[312]:


df_Sans_doublons.drop("langage4", axis=1, inplace=True)
df_Sans_doublons


# In[313]:


df_Sans_doublons['ville'] = Modif_lieu
df_Sans_doublons


# In[314]:


df_Sans_doublons.drop("lieu", axis=1, inplace=True)
df_Sans_doublons


# ### Cette fonction "rename" permet de changer le nom de deux colones suivante: "DATE" et "lien" dans la dataframe

# In[315]:


DF_Sans_Doublons= df_Sans_doublons.rename(columns={'DATE': 'date_publication','lien': 'lien_offre'})
DF_Sans_Doublons


# ### Reafficher le nombre de valeurs par catégorie à l'aide de pandas

# In[316]:


cat_col = ["nom_entreprise", "ville", "Type-contrat", "langage_1", "langage_2"]
for col in cat_col:
    print(DF_Sans_Doublons[col].value_counts())


# In[317]:


DF_Sans_Doublons['Type-contrat'] = DF_Sans_Doublons['Type-contrat'].replace(['Clermont-Ferrand '],'CDD') 


# In[318]:


DF_Sans_Doublons['Type-contrat'].value_counts()


# In[321]:


cat_col = ["nom_entreprise", "ville", "Type-contrat", "langage_1", "langage_2", "langage_3", "langage_4"]
for col in cat_col:
    print(DF_Sans_Doublons[col].value_counts())


# In[322]:


DF_Sans_Doublons


# In[324]:


cat_col = ["nom_entreprise", "ville", "Type-contrat", "langage_1", "langage_2", "langage_3", "langage_4"]
for col in cat_col:
    print(DF_Sans_Doublons[col].value_counts())


# In[325]:


DF_Sans_Doublons.to_csv("Scrap_Monster_Clean.csv", index =False)

