#!/usr/bin/env python
# coding: utf-8

# In[3]:


import psycopg2
import pandas as pd
from psycopg2.extensions import parse_dsn
from sqlalchemy import create_engine

def create_database():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()
    engine = create_engine(db_dsn)

    Row_Monster= pd.read_csv("Scp_MONSTER.csv")
    Row_Monster.to_sql('Row_Monster', engine, if_exists='replace', index = False)
    
    Clean_Monster= pd.read_csv("Scrap_Monster_Clean.csv")
    Clean_Monster.to_sql('Clean_Monster', engine, if_exists='replace', index = False)

    Pole_Emploi = pd.read_csv("DATA_PE.csv")
    Pole_Emploi.to_sql('indeed', engine, if_exists='replace', index = False)

    conn.commit()
    cur.close()
    conn.close()


# In[4]:


create_database()

