#!/usr/bin/env python
# coding: utf-8

# In[8]:


import pandas as pd

def Sup_Doublons():
    ''' permet de supprimer les doublons dans la dataframe avec les données brutes'''
    df_Avec_doublons = pd.read_csv("Coord.csv")
    df_Sans_doublons = df_Avec_doublons.drop_duplicates(subset= ["intitule_offre","nom_entreprise","lieu","DATE"],keep='first',inplace=False)
    df_Sans_doublons.to_csv("Scrap_MONSTER_sans_doublon.csv", index=False)
    
    return df_Sans_doublons


# In[10]:


Sup_Doublons()


# In[ ]:




